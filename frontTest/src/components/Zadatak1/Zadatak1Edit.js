import React, { useCallback, useEffect, useState } from 'react';
import TestAxios from '../../apis/TestAxios';
import {Col, Row, Form, Button} from 'react-bootstrap';
import { useNavigate, useParams } from 'react-router-dom';
import DatePicker from 'react-datepicker'
import 'react-datepicker/dist/react-datepicker.css'
import { getMonth, getYear } from 'date-fns';
import range from "lodash/range";

const Zadatak1Edit = (props) => {

    let init = {
        id: "",
        ime: "",
        prezime: "",
        datumRodenja: new Date(),
        mesto: "",
        pol: "",
        lbo: "",
        zadatak2dto: "",
        zadatak2Id: ""
    }

    const routeParams = useParams();
    const zadaciId = routeParams.id; 
    const navigate = useNavigate()

    const [editZadatak1, setEditZadatak1] = useState(init);
    const [zadatak2, setZadatak2] = useState([])
    const [pol, setPol] = useState([])

    useEffect(()=>{
        getZadatak1()
        getZadatak2()
        getPol()
        console.log(editZadatak1)
    }, [])

    const getZadatak1 = useCallback(() => {
        TestAxios.get('/zadatak1/' + zadaciId)
            .then(res => {
                // handle success
                console.log(res)
                let zadatak = {
                  id: res.data.id,
                  ime: res.data.ime,
                  prezime: res.data.prezime,
                  datumRodenja: new Date(res.data.datumRodenja),
                  mesto: res.data.mesto,
                  pol: res.data.pol,
                  lbo: res.data.lbo,
                  zadatak2dto: res.data.zadatak2dto
              }
                setEditZadatak1(zadatak)
                //console.log(editZadatak1)
            })
            .catch(error => {
                // handle error
                console.log(error)
                alert('Error occured please try again!')
            });
    },[])

    const getZadatak2 = useCallback(() => {
        TestAxios.get('/zadatak2')
        .then(res => {
            // handle success
            console.log(res)
            setZadatak2(res.data)
        },[])
        .catch(error => {
            // handle error
            console.log(error)
            alert('Error occured please try again!')
        });
        },[])


        const getPol= useCallback(() => {
          TestAxios.get('/enum')
          .then(res => {
              // handle success
              console.log(res)
              setPol(res.data)
          })
          .catch(error => {
              // handle error
              console.log(error)
              alert('Error occured please try again!')
          });
          },[])

            const edit = () => {
                let zadatakDTO = {
                    id: editZadatak1.id,
                    ime: editZadatak1.ime,
                    prezime: editZadatak1.prezime,
                    datumRodenja: editZadatak1.datumRodenja,
                    mesto: editZadatak1.mesto,
                    pol: editZadatak1.pol,
                    lbo: editZadatak1.lbo,
                    zadatak2Id: editZadatak1.zadatak2Id
                }
                console.log(zadatakDTO)
                TestAxios.put("/zadatak1/" + zadaciId, zadatakDTO)
                .then(() => {
                    navigate("/zadatak1");
                })
                .catch(error => {
                    console.log(error)
                    alert('Error while creating zadatak')
                })
            }


    const onChange = (e) => {
        const name = e.target.name;
        const value = e.target.value;
        editZadatak1[name] = value;
        setEditZadatak1(editZadatak1);
        console.log(editZadatak1);
      };

      const renderZadatak2 = () => {
        return zadatak2.map((zad)=>
            <option key={zad.id} value={zad.id}>{zad.ime} {zad.prezime}</option>)

      }

      const renderPol = () => {
        return pol.map((sex, index)=><option key={index} value={sex}>{sex}</option>)
    }

    //+++++++++++++++++++++++++++++++++ datepicker ++++++++++++++++++++++++
    //const [startDate, setStartDate] = useState(new Date("2000-11-11"));
    const years = range(1990, getYear(new Date()) + 1, 1);
    const months = [
      "January",
      "February",
      "March",
      "April",
      "May",
      "June",
      "July",
      "August",
      "September",
      "October",
      "November",
      "December",
    ];

    const onDateChange = (datum1) => {
      let noviZadatak2 = {... editZadatak1}
      noviZadatak2["datumRodenja"] = new Date(datum1);
      setEditZadatak1(noviZadatak2);
      console.log(editZadatak1);
    }

  const onDate = () => {
  return (
    <DatePicker
      renderCustomHeader={({
        date,
        changeYear,
        changeMonth,
        decreaseMonth,
        increaseMonth,
        prevMonthButtonDisabled,
        nextMonthButtonDisabled,
      }) => (
        <div
          style={{
            margin: 10,
            display: "flex",
            justifyContent: "center",
          }}
        >
          {/*<button onClick={decreaseMonth} disabled={prevMonthButtonDisabled}>
            {"<"}
        </button> */}
          <select
            value={getYear(date)}
            onChange={({ target: { value } }) => changeYear(value)}
          >
            {years.map((option) => (
              <option key={option} value={option}>
                {option}
              </option>
            ))}
          </select>

          <select
            value={months[getMonth(date)]}
            onChange={({ target: { value } }) =>
              changeMonth(months.indexOf(value))
            }
          >
            {months.map((option) => (
              <option key={option} value={option}>
                {option}
              </option>
            ))}
          </select>

          {/*<button onClick={increaseMonth} disabled={nextMonthButtonDisabled}>
            {">"}
          </button>*/}
        </div>
      )}
      dateFormat="dd-MM-yyyy"
      selected={editZadatak1.datumRodenja}
      onChange={(date) => onDateChange(date)}
    />
  );
};
//+++++++++++++++++++++++++++++++++ datepicker ++++++++++++++++++++++++


return(
        <>
            <Row><h1>Izmena</h1></Row>
        
            <Row>
            <Col xs="12" sm="10" md="8">
            <Form>
            <Form.Label htmlFor="ime">Ime</Form.Label>
            <Form.Control
              id="ime"
              name="ime"
              type="text"
              defaultValue={editZadatak1.ime}
              onChange={(e) => onChange(e)}
            />
            
            <Form.Label htmlFor="prezime">Prezime</Form.Label>
            <Form.Control
              id="prezime"
              name="prezime"
              type="text"
              defaultValue={editZadatak1.prezime}
              onChange={(e) => onChange(e)}
            />
            <Form.Label htmlFor="datum">Datum rodenja</Form.Label>
            
            {/*<Form.Control
              id="datum"
              name="datum"
              type="text"
              defaultValue={editZadatak1.datumRodenja}
              onChange={(e) => onChange(e)}
            /> */}
            <br/>
            {onDate()}
            <br/>
            <Form.Label htmlFor="mesto">Mesto</Form.Label>
            <Form.Control
              id="mesto"
              name="mesto"
              type="text"
              defaultValue={editZadatak1.mesto}
              onChange={(e) => onChange(e)}
            />
            
            <Form.Label htmlFor="pol">Pol</Form.Label>
            <Form.Control
              name="pol"
              as="select"
              value={editZadatak1.pol}
              onChange={(e) => onChange(e)}
            >
                {renderPol()}
            </Form.Control>
            <Form.Label htmlFor="lbo">LBO</Form.Label>
            <Form.Control
              id="lbo"
              name="lbo"
              type="text"
              defaultValue={editZadatak1.lbo}
              onChange={(e) => onChange(e)}
            />
            <Form.Label>Doktor</Form.Label>
            {editZadatak1.zadatak2Id = editZadatak1.zadatak2dto.id} {/* Bez ovog reda zadatak2Id je prazan string, ako ne diram padajuci meni */}
            <Form.Control as="select" name="zadatak2Id" value={editZadatak1.zadatak2dto.id} onChange={(e) => onChange(e)}>
                {renderZadatak2()}
            </Form.Control><br />
            <Button style={{ marginTop: "25px" }} onClick={() => edit()}>
              Edit
            </Button>
            </Form>
            </Col>
            </Row>
        </>        
);

}

export default Zadatak1Edit