import { Button } from "react-bootstrap";
import { useNavigate } from "react-router-dom";


const Zadatak1Row = (props) => {

    var navigate = useNavigate()

    // const getGenresStringFromList = (list) => {
    //     return list.map(element => element.naziv).join(',');
    // }

    const goToEdit = (id) => {
        navigate('/zadatak1/edit/' + id);
    }

    const getGenresStringFromList = (list) => {
        return list.map(element => element.ime + " " + element.prezime).join(' ');
    }

    return (
        <tr>
           {/*{console.log("Unutar row: " + props.zadatak.ime)} */}
           <td>{props.zadatak.ime}</td>
           <td>{props.zadatak.prezime}</td>
           <td>{props.zadatak.datumRodenja}</td>
           <td>{props.zadatak.mesto}</td>
           <td>{props.zadatak.pol}</td>
           <td>{props.zadatak.lbo}</td>
           <td>{props.zadatak.zadatak2dto.ime} {props.zadatak.zadatak2dto.prezime}</td>
           {/*console.log("Unutar row: " + props.zadatak.zadatak2dto.ime + props.zadatak.zadatak2dto.prezime)*/}
           {/*<td>{getGenresStringFromList(props.zadatak.zadatak2dto)}</td>*/}
           {window.localStorage['svoja rola']=='ADMIN'?
           <td><Button variant="warning" onClick={() => goToEdit(props.zadatak.id)}>Edit</Button></td> : null}
           {window.localStorage['svoja rola']=='ADMIN'?
           <td><Button variant="danger" onClick={() => props.deleteZadatakCallback(props.zadatak.id)}>Delete</Button></td> : null}
        </tr>
     )
}

export default Zadatak1Row;