import React, { useCallback, useEffect, useState } from 'react';
import TestAxios from '../../apis/TestAxios';
import { Row, Col, Button, Table, Form } from 'react-bootstrap'
import { useNavigate } from 'react-router-dom';
import Zadatak1Row from "./Zadatak1Row"
import DatePicker from 'react-datepicker'
import 'react-datepicker/dist/react-datepicker.css'
import { getMonth, getYear } from 'date-fns';
import range from "lodash/range";

const Zadatak1 = (props) => {

    let init = {
        id: "",
        ime: "",
        prezime: "",
        datum: new Date(),
        mesto: "",
        pol: "",
        lbo: "",
        polList: "",
        zadatak2dto: "",
        zadatak2Id: ""
    }

    let empty_search = {
        zadatak2Id: "",
        lbo: "",
        ime: "",
        prezime: ""
    }

    const [totalPages, setTotalPages] = useState(0)
    const [pageNo, setPageNo] = useState(0)
    const [showSearch, setShowSearch] = useState(false)
    const [search, setSearch] = useState(empty_search)
    const [zadatak1, setZadatak1] = useState([])
    const [noviZadatak, setNoviZadatak] = useState(init)
    const [zadatak2, setZadatak2] = useState([])
    const [pol, setPol] = useState([])
    

    useEffect(()=>{
        getZadatak1(0);
        getZadatak2();
        getPol();
    }, [])

    const getZadatak1 = useCallback((newPageNo) => {
        let config = {
            params: {
                pageNo: newPageNo,
                zadatak2Id: search.zadatak2Id,
                lbo: search.lbo,
                ime: search.ime,
                prezime: search.prezime
            }
        }
        TestAxios.get('/zadatak1', config)
            .then(res => {
                // handle success
                console.log(res)
                setPageNo(newPageNo)
                setTotalPages(res.headers['total-pages'])
                setZadatak1(res.data)
                console.log("total pages: " + totalPages)
            })
            .catch(error => {
                // handle error
                console.log(error)
                alert('Error occured please try again!')
            });
    },[])

    const getZadatak2= useCallback(() => {
    TestAxios.get('/zadatak2')
    .then(res => {
        // handle success
        console.log(res)
        setZadatak2(res.data)
    })
    .catch(error => {
        // handle error
        console.log(error)
        alert('Error occured please try again!')
    });
    },[])

    const getPol= useCallback(() => {
        TestAxios.get('/enum')
        .then(res => {
            // handle success
            console.log(res)
            setPol(res.data)
        })
        .catch(error => {
            // handle error
            console.log(error)
            alert('Error occured please try again!')
        });
        },[])



    const create = () => {
        let zadatakDTO = {
            ime: noviZadatak.ime,
            prezime: noviZadatak.prezime,
            datumRodenja: noviZadatak.datum,
            mesto: noviZadatak.mesto,
            pol: noviZadatak.pol,
            lbo: noviZadatak.lbo,
            zadatak2Id: noviZadatak.zadatak2Id
        }
        console.log("Create: " + zadatakDTO.lbo);
        console.log("Create: " + zadatakDTO.datumRodenja);
        console.log(zadatakDTO);
        TestAxios.post("/zadatak1", zadatakDTO)
        .then(() => {
            window.location.reload();
        })
        .catch(error => {
            console.log(error)
            alert('Error while creating projection')
        })
    }


    const deleteZadatak = (zadatakId) => {
        TestAxios.delete('/zadatak1/' + zadatakId)
            .then(res => {
                // handle success
                console.log(res)
                window.location.reload();
            })
            .catch(error => {
                // handle error
                console.log(error)
                alert('Error occured please try again!')
            });
    }

    const onChangeInput = (e) => {
        let noviZadatak1 = {... noviZadatak}
        const name = e.target.name;
        const value = e.target.value;
        noviZadatak1[name] = value;
        setNoviZadatak(noviZadatak1);
        
        //console.log(dozvoljenaAdd());
        console.log(noviZadatak);
      };

    const onChangeSearch = (e) => {
        const name = e.target.name;
        const value = e.target.value;
        search[name] = value;
        setSearch(search);
        //console.log(search);
    };
    
    //++++++++++++++++ datePickup+++++++++++++++++++

    const [startDate, setStartDate] = useState(new Date());

    const years = range(1900, getYear(new Date()) + 1, 1);
    const months = [
          "January",
          "February",
          "March",
          "April",
          "May",
          "June",
          "July",
          "August",
          "September",
          "October",
          "November",
          "December",
        ];

    const onDateChange = (datum1) => {
      let noviZadatak2 = {... noviZadatak}
      noviZadatak2["datum"] = new Date(datum1);
      setNoviZadatak(noviZadatak2);
      console.log(noviZadatak);
    }

    const onDate  = () => {
        return (
          <DatePicker
            renderCustomHeader={({
              date,
              changeYear,
              changeMonth,
              decreaseMonth,
              increaseMonth,
              prevMonthButtonDisabled,
              nextMonthButtonDisabled,
            }) => (
              <div
                style={{
                  margin: 10,
                  display: "flex",
                  justifyContent: "center",
                }}
              >
                {/*<button onClick={decreaseMonth} disabled={prevMonthButtonDisabled}>
                  {"<"}
                </button> */}
                <select
                  value={getYear(date)}
                  onChange={({ target: { value } }) => changeYear(value)}
                >
                  {years.map((option) => (
                    <option key={option} value={option}>
                      {option}
                    </option>
                  ))}
                </select>
      
                <select
                  value={months[getMonth(date)]}
                  onChange={({ target: { value } }) =>
                    changeMonth(months.indexOf(value))
                  }
                >
                  {months.map((option) => (
                    <option key={option} value={option}>
                      {option}
                    </option>
                  ))}
                </select>
      
                {/*<button onClick={increaseMonth} disabled={nextMonthButtonDisabled}>
                  {">"}
                </button>*/}
              </div>
            )}
            dateFormat="dd-MM-yyyy"
            selected={startDate}
            onChange={(date) => {setStartDate(date); onDateChange(date);}}       
          />
        );
      };

//++++++++++++++++ datePickup+++++++++++++++++++


    const renderZad = () => {
        return zadatak1.map((zadatak) => {
            //{console.log("zadatak: " + zadatak.ime)}
            return <Zadatak1Row key={zadatak.id} zadatak={zadatak} deleteZadatakCallback={deleteZadatak}></Zadatak1Row>
        })
    }

    const renderZadatak2 = () => {
        return zadatak2.map((zad)=><option key={zad.id} value={zad.id}>{zad.ime} {zad.prezime}</option>)
    }

    const renderPol = () => {
        return pol.map((sex, index)=><option key={index} value={sex}>{sex}</option>)
    }


    let dozvoljenaAdd = () => {
        return noviZadatak.ime != "" &&
                noviZadatak.prezime != "" &&
                noviZadatak.datumRodenja != "" &&
                noviZadatak.mesto != "" &&
                noviZadatak.pol != "" &&
                noviZadatak.zadatak2Id != "" &&
                noviZadatak.lbo.length>0 && noviZadatak.lbo.length<=11
    }

    const renderSearchForm = () => {
        return (
            <>
            <Form style={{ width: "99%" }}>
                <Row><Col>
                    <Form.Group>
                        <Form.Label>Ime</Form.Label>
                        <Form.Control
                            name="ime"
                            as="input"
                            type="text"
                            onChange={(e) => onChangeSearch(e)}></Form.Control>
                        <Form.Label>Prezime</Form.Label>
                        <Form.Control
                            name="prezime"
                            as="input"
                            type="text"
                            onChange={(e) => onChangeSearch(e)}></Form.Control>
                        <Form.Label>LBO</Form.Label>
                        <Form.Control
                            name="LBO"
                            as="input"
                            type="text"
                            onChange={(e) => onChangeSearch(e)}></Form.Control>
                    </Form.Group>
                </Col></Row>    

                <Row>
                    <Col>
                        <Form.Group>
                            <Form.Label>Doktor</Form.Label>
                            <Form.Control as="select" 
                                name="zadatak2Id" 
                                onChange={(e) => onChangeSearch(e)}>
                                <option></option>
                                {renderZadatak2()}
                            </Form.Control><br />
                        </Form.Group>
                    </Col>
                </Row>
                
            </Form>
            <Row><Col>
                <Button className="mt-3" onClick={() => getZadatak1(0)}>Search</Button>
            </Col></Row>
            </>
        );
    }



    return (
        
        <Col>
            <Row><h1>Lekarski pregled</h1></Row>

            {window.localStorage['svoja rola']=='ADMIN'?
            <Row>
            <Col xs="12" sm="10" md="8">
            <Form>
            
            <Form.Label htmlFor="ime">Ime</Form.Label>
            <Form.Control
              id="ime"
              name="ime"
              type="text"
              onChange={(e) => onChangeInput(e)}
              value={noviZadatak.ime}
            />
            
            <Form.Label htmlFor="prezime">Prezime</Form.Label>
            <Form.Control
              id="prezime"
              name="prezime"
              type="text"
              onChange={(e) => onChangeInput(e)}
              value={noviZadatak.prezime}
            />
            <Form.Label htmlFor="datum">Datum rodenja</Form.Label>
            <br/>
              {onDate()}
            <br/>
            <Form.Label htmlFor="mesto">Mesto</Form.Label>
            <Form.Control
              id="mesto"
              name="mesto"
              type="text"
              onChange={(e) => onChangeInput(e)}
              value={noviZadatak.mesto}
            />
            
            <Form.Label htmlFor="pol">Pol</Form.Label>
            <Form.Control
              name="pol"
              as="select"
              onChange={(e) => onChangeInput(e)}
            >
                <option></option>
                {renderPol()}
            </Form.Control>
            <Form.Label htmlFor="lbo">LBO</Form.Label>
            <Form.Control
              id="lbo"
              name="lbo"
              type="text"
              onChange={(e) => onChangeInput(e)}
              value={noviZadatak.lbo}
            />          
            <Form.Label htmlFor="zadatak2Id">Doktor</Form.Label>
            <Form.Control as="select" name="zadatak2Id" onChange={(e) => onChangeInput(e)}>
                <option></option>
                {renderZadatak2()}
            </Form.Control><br />
            {console.log(dozvoljenaAdd())}
            <Button disabled={!dozvoljenaAdd()} style={{ marginTop: "25px" }} onClick={() => create()}>
              Add
            </Button>
            </Form>
            </Col>
            </Row>: null}
            <br/>
            <br/> 

            <div>
                <label>
                <input type="checkbox" onChange={()=>setShowSearch(!showSearch)}/>
                    Show Search
                </label>
            </div>
            <Row hidden={!showSearch} >
                {renderSearchForm()}
            </Row>
            <br/>
            <br/>

            <Button disabled={pageNo===0} onClick={()=>getZadatak1(pageNo-1)} className="mr-3">Prev</Button>
            <Button disabled={pageNo==totalPages-1} onClick={()=>getZadatak1(pageNo+1)}>Next</Button>
            <Row><Col>
            <Table style={{marginTop: 5}}>
            <thead>
            <tr>
            <th>Ime</th>
            <th>Prezime (min)</th>
            <th>Datum</th>
            <th>Mesto</th>
            <th>Pol</th>
            <th>LBO</th>
            <th>Doktor</th>
            <th></th>
            </tr>
            </thead>
            <tbody>
                {renderZad()}
            </tbody>
            </Table>
            </Col></Row>
        </Col>
    );
}

export default Zadatak1