import React from 'react'
import { Carousel } from 'react-bootstrap';
import Cinema1 from './Slike/cinema1.jpg';
import Cinema2 from './Slike/cinema2.jpg';
import Cinema3 from './Slike/cinema3.jpg';


class Home extends React.Component {
    render() {
  
      return (
      <Carousel>
        <Carousel.Item>
        <img 
          className="d-block w-100"
          src={Cinema1}
          alt="First slide"
        />
          <Carousel.Caption>
            <h3>First slide label</h3>
            <p>Nulla vitae elit libero, a pharetra augue mollis interdum.</p>
          </Carousel.Caption>
        </Carousel.Item>
        <Carousel.Item>
        <img 
          className="d-block w-100"
          src={Cinema2}
          alt="Second slide"
        />
          <Carousel.Caption>
            <h3>Second slide label</h3>
            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
          </Carousel.Caption>
        </Carousel.Item>
        <Carousel.Item>
        <img 
          className="d-block w-100"
          src={Cinema3}
          alt="Third slide"
        />
          <Carousel.Caption>
            <h3>Third slide label</h3>
            <p>
              Praesent commodo cursus magna, vel scelerisque nisl consectetur.
            </p>
          </Carousel.Caption>
        </Carousel.Item>
      </Carousel>
    );
  }
  }

export default Home;