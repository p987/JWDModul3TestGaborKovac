import TestAxios from '../apis/TestAxios'
import {jwtDecode} from "jwt-decode"

export const login = async function(username, password){
    const cred = {
        username: username,
        password: password
    }

    try{
        const ret = await TestAxios.post('korisnici/auth',cred);
        const jwt_decoded = jwtDecode(ret.data);
        window.localStorage.setItem('jwt',ret.data);
        window.localStorage.setItem('role',jwt_decoded.role.authority);
        window.localStorage.setItem('user',jwt_decoded.sub);
        console.log("jwtdecode " + jwt_decoded);
        const authorizacija = jwt_decoded.role.authority.trim().substr(5);
        window.localStorage.setItem('svoja rola',authorizacija);
        // window.location.replace("http://localhost:3000")
        window.location.reload();
    }catch(err){
        alert("Neuspesan login");
        console.log(err);
    }
}

export const logout = function(){
    window.localStorage.removeItem('jwt');
    window.localStorage.removeItem('role');
    window.localStorage.removeItem('user');
    window.localStorage.removeItem('svoja rola');
    window.location.reload();
}
