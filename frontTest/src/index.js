import React, { useState } from 'react';
import { createRoot } from 'react-dom/client';
import { Route, Link, BrowserRouter as Router, Routes, Navigate } from 'react-router-dom';
import { Navbar, Nav, Container} from 'react-bootstrap';
import Home from './components/Home';
import Login from './components/authorization/Login'
import Zadatak1Add from './components/Zadatak1/Zadatak1Add';
import Zadatak1Edit from './components/Zadatak1/Zadatak1Edit';
import Zadatak1 from './components/Zadatak1/Zadatak1';
import Projections from './components/projections/Projections';
import AddProjection from './components/projections/AddProjection';
import NotFound from './components/NotFound';
import {logout} from './services/auth';

const App = () => {

    const jwt = window.localStorage['jwt'];
    const user = window.localStorage['user'];

    if(jwt){
        return (
        <>
            <Router>
                {console.log("User: " +user)}
                <Navbar expand bg="dark" variant="dark">
                    <Navbar.Brand as={Link} to="/">
                        JWD
                    </Navbar.Brand>
                    <Nav className="mr-auto">
                    <Nav.Link as={Link} to="/zadatak1">
                        Zadatak1
                    </Nav.Link> 
                    </Nav>

                    
                    <Container>
                        <Navbar.Toggle />
                        <Navbar.Collapse className="justify-content-end">
                            <Navbar.Text>
                            <i className="fa fa-user-circle-o" aria-hidden="true" style={{color : 'lime'}}>&nbsp;&nbsp;</i>
                            Signed in as : 
                            <a href="/" onClick={() => logout()} > {user}</a>     
                            </Navbar.Text>
                        </Navbar.Collapse>
                    </Container>
                    </Navbar>
                
                <Container style={{marginTop:25}}>
                <Routes>
                    <Route path="/" element={<Home/>} />
                    <Route path="/login" element={<Navigate replace to='/'/>} />
                    <Route path="/zadatak1" element={<Zadatak1/>} />
                    {/*<Route path="/zadatak1/add" element={<Zadatak1Add/>} />*/}
                    <Route path="/zadatak1/edit/:id" element={<Zadatak1Edit/>} />
                    <Route path="/projections" element={<Projections/>} />
                    <Route path="/projections/add" element={<AddProjection/>} />
                    <Route path="*" element={<NotFound/>} />
                </Routes>
            </Container>
            </Router>
        </>
    );
    }else{
        return( 
        <>
            <Router>
            <Navbar expand bg="dark" variant="dark">
                    <Navbar.Brand as={Link} to="/">
                        JWD
                    </Navbar.Brand>
                    <Nav className="mr-auto">
                    <Nav.Link as={Link} to="/zadatak1">
                        Zadatak1
                    </Nav.Link>
                    {/*ovde ubaci*/}
                    </Nav>

                    
                    <Container>
                        <Navbar.Toggle />
                        <Navbar.Collapse className="justify-content-end">
                            <Navbar.Text>
                            <i className="fa fa-user-circle-o" aria-hidden="true" style={{color : 'red'}}>&nbsp;&nbsp;</i>
                            <a href="/login">Login</a>   
                            </Navbar.Text>
                        </Navbar.Collapse>
                    </Container>
            </Navbar>

                <Container style={{marginTop:25}}>
                <Routes>
                    <Route path="/" element={<Home/>} />
                    <Route path="/login" element={<Login/>}/>
                    <Route path="/zadatak1" element={<Zadatak1/>} />
                    <Route path="*" element={<Navigate replace to = "/login"/>}/>
                </Routes>
                </Container>
            </Router>
        </>);
    }
}

const rootElement = document.getElementById('root');
const root = createRoot(rootElement);

root.render(
    <App/>,
)