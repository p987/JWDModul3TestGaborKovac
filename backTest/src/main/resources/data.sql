INSERT INTO korisnik (id, e_mail, korisnicko_ime, lozinka, ime, prezime, uloga)
              VALUES (1,'miroslav@maildrop.cc','miroslav','$2y$12$NH2KM2BJaBl.ik90Z1YqAOjoPgSd0ns/bF.7WedMxZ54OhWQNNnh6','Miroslav','Simic','ADMIN');
INSERT INTO korisnik (id, e_mail, korisnicko_ime, lozinka, ime, prezime, uloga)
              VALUES (2,'tamara@maildrop.cc','tamara','$2y$12$DRhCpltZygkA7EZ2WeWIbewWBjLE0KYiUO.tHDUaJNMpsHxXEw9Ky','Tamara','Milosavljevic','KORISNIK');
INSERT INTO korisnik (id, e_mail, korisnicko_ime, lozinka, ime, prezime, uloga)
              VALUES (3,'petar@maildrop.cc','petar','$2y$12$i6/mU4w0HhG8RQRXHjNCa.tG2OwGSVXb0GYUnf8MZUdeadE4voHbC','Petar','Jovic','KORISNIK');

INSERT INTO zadatak2 (id, ime, prezime, specijalizacija) VALUES (1, 'Radovan', 'Mikic', 'Neurolog');
INSERT INTO zadatak2 (id, ime, prezime, specijalizacija) VALUES (2, 'Pera', 'Peric', 'Kardiolog');
INSERT INTO zadatak2 (id, ime, prezime, specijalizacija) VALUES (3, 'Vesna', 'Nemanjic', 'Opsat praksa');

INSERT INTO zadatak1 (id, lbo, datum_rodenja, ime, mesto, pol, prezime, zadatak2_id) VALUES (1, '111', '2020-06-21', 'Bojan', 'Novi Sad', 'muski', 'Pekaric', 1);
INSERT INTO zadatak1 (id, lbo, datum_rodenja, ime, mesto, pol, prezime, zadatak2_id) VALUES (2, '222', '2020-07-22', 'Bojana', 'Beograd', 'zenski', 'Nikolic', 2);
INSERT INTO zadatak1 (id, lbo, datum_rodenja, ime, mesto, pol, prezime, zadatak2_id) VALUES (3, '333', '2020-06-22', 'Nenad', 'Zrenjanin', 'muski', 'Kovac', 2);
INSERT INTO zadatak1 (id, lbo, datum_rodenja, ime, mesto, pol, prezime, zadatak2_id) VALUES (4, '123', '1985-06-22', 'Tamara', 'NoviSad', 'zenski', 'Peric', 3);
INSERT INTO zadatak1 (id, lbo, datum_rodenja, ime, mesto, pol, prezime, zadatak2_id) VALUES (5, '444', '1998-05-12', 'Zoran', 'Beograd', 'muski', 'Maric', 1);

