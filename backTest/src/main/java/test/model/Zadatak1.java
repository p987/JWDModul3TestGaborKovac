package test.model;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

import test.enumeration.Pol;


@Entity
//@Table(name= "filmovi")
public class Zadatak1 {
	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
	
	@Column(nullable=false)
    private String ime;
	
	@Column(nullable=false)
    private String prezime;
	
	@Column(nullable=false)
    private LocalDate datumRodenja;
	
	@Column
    private String mesto;
	
	@Column
	@Enumerated(EnumType.STRING)
    private Pol pol;
	
	@ManyToOne
	private Zadatak2 zadatak2;
	
	@Column(nullable=false, unique=true)
    private String LBO;
	
	
	
	public Zadatak1() {
	}
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getIme() {
		return ime;
	}

	public void setIme(String ime) {
		this.ime = ime;
	}

	public String getPrezime() {
		return prezime;
	}

	public void setPrezime(String prezime) {
		this.prezime = prezime;
	}

	public LocalDate getDatumRodenja() {
		return datumRodenja;
	}

	public void setDatumRodenja(LocalDate datumRodenja) {
		this.datumRodenja = datumRodenja;
	}

	public String getMesto() {
		return mesto;
	}

	public void setMesto(String mesto) {
		this.mesto = mesto;
	}

	public Pol getPol() {
		return pol;
	}

	public void setPol(Pol pol) {
		this.pol = pol;
	}

	public Zadatak2 getZadatak2() {
		return zadatak2;
	}

	public void setZadatak2(Zadatak2 zadatak2) {
		this.zadatak2 = zadatak2;
	}

	public String getLBO() {
		return LBO;
	}

	public void setLBO(String lBO) {
		LBO = lBO;
	}
	
	
}
