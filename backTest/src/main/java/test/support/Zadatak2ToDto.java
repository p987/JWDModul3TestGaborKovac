package test.support;

import java.util.ArrayList;
import java.util.List;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import test.dto.Zadatak1DTO;
import test.dto.Zadatak2DTO;
import test.model.Zadatak1;
import test.model.Zadatak2;


@Component
public class Zadatak2ToDto implements Converter<Zadatak2, Zadatak2DTO>{

	@Override
	public Zadatak2DTO convert(Zadatak2 zadatak) {
		Zadatak2DTO dto=new Zadatak2DTO();
		dto.setId(zadatak.getId());
		dto.setIme(zadatak.getIme());
		dto.setPrezime(zadatak.getPrezime());
		dto.setSpecijalizacija(zadatak.getSpecijalizacija());
		
		return dto;
	}

	public List<Zadatak2DTO>convert(List<Zadatak2>zadaci){
		List<Zadatak2DTO> zadaciDTO=new ArrayList<>();
		
		for(Zadatak2 zadatak:zadaci) {
			zadaciDTO.add(convert(zadatak));
		}
		return zadaciDTO;
	}
}
