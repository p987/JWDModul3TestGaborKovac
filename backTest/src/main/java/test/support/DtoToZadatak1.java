package test.support;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;

import org.hibernate.internal.build.AllowSysOut;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.format.datetime.joda.LocalDateParser;
import org.springframework.stereotype.Component;

import test.dto.Zadatak1DTO;
import test.model.Zadatak1;
import test.service.Zadatak1Service;
import test.service.Zadatak2Service;

@Component
public class DtoToZadatak1 implements Converter<Zadatak1DTO, Zadatak1> {

	@Autowired
	Zadatak1Service zadatakService;
	
	@Autowired
	Zadatak2Service zadatak2Service;
	
	@Override
	public Zadatak1 convert(Zadatak1DTO dto) {
		Zadatak1 entity;
		if(dto.getId()==null) {
			entity=new Zadatak1();
		}else{
			entity = zadatakService.getOne(dto.getId());
		}
		
		if(entity !=null) {
			
			entity.setIme(dto.getIme());
			entity.setPrezime(dto.getPrezime());
			entity.setDatumRodenja(getLocalDate(dto.getDatumRodenja()));
			entity.setMesto(dto.getMesto());
			entity.setPol(dto.getPol());
			entity.setLBO(dto.getLbo());
			entity.setZadatak2(zadatak2Service.getOne(dto.getZadatak2Id()));
			
			
		}
		
		return entity;
	}

	
	 private LocalDate getLocalDate(String datum) throws DateTimeParseException {
	        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
	        try {
		        LocalDate datumOk = LocalDate.parse(datum.substring(0, 10), formatter);
		        return datumOk;
			} catch (Exception e) {
				e.printStackTrace();
				return null;
			}

	    }
}
