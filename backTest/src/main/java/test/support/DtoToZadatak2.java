package test.support;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import test.dto.Zadatak2DTO;
import test.model.Zadatak2;
import test.service.Zadatak2Service;

@Component
public class DtoToZadatak2 implements Converter<Zadatak2DTO, Zadatak2> {

	@Autowired
	Zadatak2Service zadatakService;
	
	@Override
	public Zadatak2 convert(Zadatak2DTO dto) {
		Zadatak2 entity;
		if(dto.getId()==null) {
			entity=new Zadatak2();
		}else{
			entity = zadatakService.getOne(dto.getId());
		}
		
		if(entity !=null) {
			
			entity.setIme(dto.getIme());
			entity.setPrezime(dto.getPrezime());
			entity.setSpecijalizacija(dto.getSpecijalizacija());
			
		}
		
		return entity;
	}

}
