package test.support;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import test.dto.Zadatak1DTO;
import test.dto.Zadatak2DTO;
import test.enumeration.Pol;
import test.model.Zadatak1;


@Component
public class Zadatak1ToDto implements Converter<Zadatak1, Zadatak1DTO> {
	
	@Autowired
	Zadatak2ToDto zadatak2dto;

	@Override
	public Zadatak1DTO convert(Zadatak1 zadatak) {
		Zadatak1DTO dto=new Zadatak1DTO();
		dto.setId(zadatak.getId());
		dto.setIme(zadatak.getIme());
		dto.setPrezime(zadatak.getPrezime());
		dto.setDatumRodenja(zadatak.getDatumRodenja().toString());
		dto.setMesto(zadatak.getMesto());
		dto.setPol(zadatak.getPol());
		dto.setLbo(zadatak.getLBO());
		dto.setZadatak2dto(zadatak2dto.convert(zadatak.getZadatak2()));
//		dto.setPolList(Arrays.asList(Pol.values()));
		
		return dto;
	}

	public List<Zadatak1DTO>convert(List<Zadatak1>zadaci){
		List<Zadatak1DTO> zadaciDTO=new ArrayList<>();
		
		for(Zadatak1 zadatak:zadaci) {
			zadaciDTO.add(convert(zadatak));
		}
		return zadaciDTO;
	}
}