package test.service;

import java.util.List;

import test.model.Zadatak2;

public interface Zadatak2Service {
	
	List<Zadatak2> getAll();
	Zadatak2 getOne (Long id);
}
