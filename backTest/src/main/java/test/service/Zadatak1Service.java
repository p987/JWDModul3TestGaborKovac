package test.service;

import java.util.List;

import org.springframework.data.domain.Page;

import test.model.Zadatak1;

public interface Zadatak1Service {
	
	List<Zadatak1> getAll();
	Zadatak1 getOne (Long id);
	Zadatak1 getOneLbo(String lbo);
	Zadatak1 save (Zadatak1 zadatak);
	Zadatak1 update (Zadatak1 zadatak);
	Zadatak1 delete (Long id);
	Page<Zadatak1>search(String ime, String prezime, String LBO, Long zadatak2Id, int pageNo);
}
