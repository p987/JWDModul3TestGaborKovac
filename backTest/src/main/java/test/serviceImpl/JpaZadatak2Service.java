package test.serviceImpl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;

import test.model.Zadatak2;
import test.repository.Zadatak2Repository;
import test.service.Zadatak2Service;

@Service
public class JpaZadatak2Service implements Zadatak2Service {

	@Autowired
	Zadatak2Repository repository;
	
	@Override
	public List<Zadatak2> getAll() {
		return repository.findAll();
	}

	@Override
	public Zadatak2 getOne(Long id) {
		return repository.findOneById(id);
	}


}
