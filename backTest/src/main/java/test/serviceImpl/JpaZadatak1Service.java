package test.serviceImpl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import test.model.Zadatak1;
import test.repository.Zadatak1Repository;
import test.service.Zadatak1Service;

@Service
public class JpaZadatak1Service implements Zadatak1Service{

	@Autowired
	Zadatak1Repository repository;
	
	@Override
	public List<Zadatak1> getAll() {
		return repository.findAll();
	}

	@Override
	public Zadatak1 getOne(Long id) {
		return repository.findOneById(id);
	}

	@Override
	public Zadatak1 getOneLbo(String lbo) {
		return repository.findOneByLBO(lbo);
	}
	
	
	@Override
	public Zadatak1 save(Zadatak1 zadatak) {
		return repository.save(zadatak);
	}

	@Override
	public Zadatak1 update(Zadatak1 zadatak) {
		return repository.save(zadatak);
	}

	@Override
	public Zadatak1 delete(Long id) {
		Zadatak1 zadatak;
		zadatak = getOne(id);
		repository.delete(zadatak);
		return zadatak;
	}

	@Override
	public Page<Zadatak1> search(String ime, String prezime, String LBO, Long zadatak2Id, int pageNo) {
		if (ime==null) {
			ime = "";
		} 
		if (prezime==null) {
			ime = "";
		} 
		if (LBO==null) {
			ime = "";
		} 
		if(zadatak2Id==null) {
			return repository.findByImeIgnoreCaseContainsAndPrezimeIgnoreCaseContainsAndLBOIgnoreCaseContains(ime, prezime, LBO, PageRequest.of(pageNo, 4));
		}
		return repository.findByImeIgnoreCaseContainsAndPrezimeIgnoreCaseContainsAndLBOIgnoreCaseContainsAndZadatak2Id(ime, prezime,LBO, zadatak2Id, PageRequest.of(pageNo, 4));
	}

}
