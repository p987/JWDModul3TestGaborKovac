package test.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import test.model.Zadatak1;


@Repository
public interface Zadatak1Repository extends JpaRepository<Zadatak1, Long>{
	Zadatak1 findOneById (Long id);
	Zadatak1 findOneByLBO (String lbo);
	Page<Zadatak1> findByImeIgnoreCaseContainsAndPrezimeIgnoreCaseContainsAndLBOIgnoreCaseContainsAndZadatak2Id (String ime, String prezime,String LBO, Long zadatak2Id, Pageable pageable);
	//Page<Zadatak> findBySprintId (Long SprintId, Pageable pageable);
	Page<Zadatak1> findByImeIgnoreCaseContainsAndPrezimeIgnoreCaseContainsAndLBOIgnoreCaseContains(String ime, String prezime,String LBO, Pageable pageable);
}
