package test.dto;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

import test.enumeration.Pol;

public class Zadatak1DTO {
	private Long id;
	
	@NotBlank(message="Niste uneli ime.")
	@Size(max=35, message="Duzina imena zadatka moze imati maksimalno 35 karatkera.")
	private String ime;
	
	@NotBlank(message="Niste uneli prezime.")
	private String prezime;
	
//	@NotBlank(message="Niste uneli datum.")
//	@Pattern(regexp = "^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$", message = "Datum nije validan.")
	private String datumRodenja;
	
	private String mesto;
	
	private Pol pol;
	
	@NotBlank(message="Niste uneli LBO.")
	@Size(max=11, message="Duzina LBO moze imati maksimalno 11 karatkera.")
	private String lbo;
	
	private Zadatak2DTO zadatak2dto;
	
	private Long zadatak2Id;
	
//	private List<Pol> polList = new ArrayList<>(); 

	
	
	public Zadatak1DTO() {
	}
	
	
	public Long getId() {
		return id;
	}


	public void setId(Long id) {
		this.id = id;
	}


	public String getIme() {
		return ime;
	}


	public void setIme(String ime) {
		this.ime = ime;
	}


	public String getPrezime() {
		return prezime;
	}


	public void setPrezime(String prezime) {
		this.prezime = prezime;
	}


	public String getMesto() {
		return mesto;
	}


	public void setMesto(String mesto) {
		this.mesto = mesto;
	}


	public Pol getPol() {
		return pol;
	}


	public void setPol(Pol pol) {
		this.pol = pol;
	}


	public void setZadatak2Id(Long zadatak2Id) {
		this.zadatak2Id = zadatak2Id;
	}


	public Long getZadatak2Id() {
		return zadatak2Id;
	}


	public Zadatak2DTO getZadatak2dto() {
		return zadatak2dto;
	}


	public void setZadatak2dto(Zadatak2DTO zadatak2dto) {
		this.zadatak2dto = zadatak2dto;
	}


	public String getDatumRodenja() {
		return datumRodenja;
	}


	public void setDatumRodenja(String datumRodenja) {
		this.datumRodenja = datumRodenja;
	}


	public String getLbo() {
		return lbo;
	}


	public void setLbo(String lbo) {
		this.lbo = lbo;
	}


//	public List<Pol> getPolList() {
//		return polList;
//	}
//
//
//	public void setPolList(List<Pol> polList) {
//		this.polList = polList;
//	}
	
	
	
	
}
