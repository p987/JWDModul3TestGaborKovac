package test.dto;

import javax.validation.constraints.NotBlank;

public class Zadatak2DTO {
	private Long id;
	
	@NotBlank(message="Niste uneli ime.")
	private String ime;
	
	@NotBlank(message="Niste uneli prezime.")
	private String prezime;
	
	@NotBlank(message="Niste uneli specijalizaciju.")
	private String specijalizacija;
	
	
	public Zadatak2DTO() {
	}
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getIme() {
		return ime;
	}

	public void setIme(String ime) {
		this.ime = ime;
	}

	public String getPrezime() {
		return prezime;
	}

	public void setPrezime(String prezime) {
		this.prezime = prezime;
	}

	public String getSpecijalizacija() {
		return specijalizacija;
	}

	public void setSpecijalizacija(String specijalizacija) {
		this.specijalizacija = specijalizacija;
	}
	
}
