package test.controller;

import java.util.Arrays;
import java.util.List;

import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import test.enumeration.Pol;

@RestController
@RequestMapping(value="/api/enum", produces=MediaType.APPLICATION_JSON_VALUE)
public class EnumerationController {

	@PreAuthorize("permitAll()")
	@GetMapping
	public ResponseEntity<List<Pol>> getPol(){
			return new ResponseEntity<>(Arrays.asList(Pol.values()), HttpStatus.OK);
	}
}
