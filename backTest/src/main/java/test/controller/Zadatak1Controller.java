package test.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import test.dto.Zadatak1DTO;
import test.model.Zadatak1;
import test.service.Zadatak1Service;
import test.support.DtoToZadatak1;
import test.support.Zadatak1ToDto;



@RestController
@RequestMapping(value="/api/zadatak1", produces=MediaType.APPLICATION_JSON_VALUE)
@Validated
public class Zadatak1Controller {
	
	@Autowired
	Zadatak1Service zadatakService;
	
	@Autowired
	DtoToZadatak1 toZadatak;
	
	@Autowired
	Zadatak1ToDto toDto;
	
	
	
	@PreAuthorize("hasAnyRole('KORISNIK', 'ADMIN')")
    @GetMapping
	public ResponseEntity<List<Zadatak1DTO>> getAll(
			@RequestParam(required = false, defaultValue = "") String ime,
			@RequestParam(required = false, defaultValue = "") String prezime,
			@RequestParam(required = false, defaultValue = "") String LBO,
			@RequestParam(required = false, defaultValue = "") Long zadatak2Id,
			@RequestParam(required = false, defaultValue = "0", value = "pageNo") int pageNo) { 	//value treba ako pr. @RequestParam(value = "brStranica") int pageNo
																									//brStranice po tom imenom dolazi promenljiva iz fronta
		
		Page<Zadatak1> page;
		
		page = zadatakService.search(ime, prezime, LBO, zadatak2Id, pageNo);
		
		
		HttpHeaders headers = new HttpHeaders();
        headers.add("Total-Pages", Integer.toString(page.getTotalPages()));
        
        return new ResponseEntity<>(toDto.convert(page.getContent()), headers, HttpStatus.OK);
	}
	
	
	
	@PreAuthorize("hasAnyRole('KORISNIK', 'ADMIN')")
    @GetMapping("/{id}")
	public ResponseEntity<Zadatak1DTO> getOne(@PathVariable Long id) {
		
		Zadatak1 zadatak = zadatakService.getOne(id);
		
		if (zadatak != null) {
			return new ResponseEntity<>(toDto.convert(zadatak), HttpStatus.OK);
		} else {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}
	
	
	
	
	
	@PreAuthorize("hasRole('ADMIN')")
	@PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Zadatak1DTO> create(@Valid @RequestBody Zadatak1DTO zadatakDto) {
		
		Zadatak1 zadatak = toZadatak.convert(zadatakDto);
		
		if(zadatak.getZadatak2() == null) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
		
		if(zadatakService.getOneLbo(zadatak.getLBO()) != null) {
            return new ResponseEntity<>(HttpStatus.CONFLICT);
        }
		
		Zadatak1 sacuvanZadatak = zadatakService.save(zadatak);
		return new ResponseEntity<>(toDto.convert(sacuvanZadatak) , HttpStatus.CREATED);
	}
	
	
	
	
	
	@PreAuthorize("hasRole('ADMIN')")
    @PutMapping(value= "/{id}",consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Zadatak1DTO> update(@PathVariable Long id, @Valid @RequestBody Zadatak1DTO zadatakDto){
		
		if (id != zadatakDto.getId()) {
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
		
		Zadatak1 zadatak = toZadatak.convert(zadatakDto);		
		if(zadatak.getZadatak2() == null) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
		
		Zadatak1 sacuvanZadatak = zadatakService.save(zadatak);
		return new ResponseEntity<>(toDto.convert(sacuvanZadatak) , HttpStatus.OK);
	}
	
	
	
	
	
	@PreAuthorize("hasRole('ADMIN')")
    @DeleteMapping("/{id}")
    public ResponseEntity<Zadatak1DTO> delete(@PathVariable Long id){
        Zadatak1 zadatak = zadatakService.delete(id);

        if(zadatak != null) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

}
