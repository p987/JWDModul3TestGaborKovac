package test.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import test.dto.Zadatak2DTO;
import test.model.Zadatak2;
import test.service.Zadatak2Service;
import test.support.DtoToZadatak2;
import test.support.Zadatak2ToDto;


@RestController
@RequestMapping(value="/api/zadatak2", produces=MediaType.APPLICATION_JSON_VALUE)
@Validated
public class Zadatak2Controller {
	
	@Autowired
	private Zadatak2Service service;
	
	@Autowired
	private DtoToZadatak2 toZadatak2;
	
	@Autowired
	private Zadatak2ToDto toDto;
	
	
	
	@PreAuthorize("hasAnyRole('KORISNIK', 'ADMIN')")
	@GetMapping
	public ResponseEntity <List<Zadatak2DTO>> getAll(){
		
		List<Zadatak2> zadatak=service.getAll();
		
		return new ResponseEntity<>(toDto.convert(zadatak),HttpStatus.OK);
		
		}
	
	
	
	
	@PreAuthorize("hasAnyRole('KORISNIK', 'ADMIN')")
    @GetMapping("/{id}")
	public ResponseEntity<Zadatak2DTO> getOne(@PathVariable Long id) {
		
		Zadatak2 zadatak = service.getOne(id);
		
		if (zadatak != null) {
			return new ResponseEntity<>(toDto.convert(zadatak), HttpStatus.OK);
		} else {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}
}
